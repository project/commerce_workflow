<?php

namespace Drupal\commerce_workflow_order\Field;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides methods related to field definitions for order item entities.
 */
class OrderItemFieldHelper {

  /**
   * Returns additional base field definitions required by this module.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The additional base field definitions.
   */
  public static function baseFieldDefinitions() {
    $fields = [];

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setDescription(new TranslatableMarkup('The state of the order item.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'view',
        [
          'label' => 'hidden',
          'type' => 'state_transition_form',
          'settings' => [
            'require_confirmation' => TRUE,
            'use_modal' => TRUE,
          ],
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting(
        'workflow_callback',
        ['\\' . static::class, 'getWorkflowId']
      );

    return $fields;
  }

  /**
   * Returns the workflow ID for the state field of the given order item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(OrderItemInterface $order_item) {
    return \Drupal::service('entity_type.manager')
      ->getStorage('commerce_order_item_type')
      ->load($order_item->bundle())
      ->getThirdPartySetting(
        'commerce_workflow_order',
        'workflow'
      );
  }

}
