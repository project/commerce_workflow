<?php

namespace Drupal\commerce_workflow_order\Hook;

use Drupal\commerce_order\Entity\OrderItemTypeInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Holds methods related to form alterations for order items.
 */
class OrderItemTypeFormAlter {

  use StringTranslationTrait;

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new OrderItemTypeFormAlter object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(
    TranslationInterface $string_translation,
    WorkflowManagerInterface $workflow_manager
  ) {
    $this->stringTranslation = $string_translation;
    $this->workflowManager = $workflow_manager;
  }

  /**
   * Alters the order item type form to add required third party settings.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function alterForm(array &$form, FormStateInterface $form_state) {
    $workflow = $form_state
      ->getFormObject()
      ->getEntity()
      ->getThirdPartySetting(
        'commerce_workflow_order',
        'workflow'
      );
    $workflows = $this->workflowManager->getGroupedLabels('commerce_order_item');

    $form['workflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#options' => $workflows,
      '#default_value' => $workflow,
      '#description' => $this->t('Used by all order items of this type.'),
    ];

    if (!$workflows) {
      $form['workflow']['#disabled'] = TRUE;
      $form['workflow']['#description'] = $this->t(
        'No workflows have been defined for order items. Make sure that you have
         installed one or more modules that provide order item workflows.'
      );
    }

    $form['#entity_builders'][] = 'commerce_workflow_order__order_item_type__form_entity_builder';
  }

  /**
   * Sets third party settings when building the order item type in its forms.
   *
   * @param string $entity_type_id
   *   The ID of the entity type.
   * @param \Drupal\commerce_order\Entity\OrderItemTypeInterface $order_item_type
   *   The order item type configuration entity.
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildEntity(
    string $entity_type_id,
    OrderItemTypeInterface $order_item_type,
    array &$form,
    FormStateInterface $form_state
  ) {
    $order_item_type->setThirdPartySetting(
      'commerce_workflow_order',
      'workflow',
      $form_state->getValue('workflow')
    );
  }

}
