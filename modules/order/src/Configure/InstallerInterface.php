<?php

namespace Drupal\commerce_workflow_order\Configure;

/**
 * Installs configuration as expected by Commerce Workflow Order functionality.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. This way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 */
interface InstallerInterface {

  /**
   * Installs all order-related configuration.
   *
   * It includes:
   *   - Installing the `state` order item base field.
   */
  public function install();

}
