<?php

namespace Drupal\commerce_workflow_order\Configure;

use Drupal\commerce_workflow_order\Field\OrderItemFieldHelper;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;

/**
 * Default implementation of the Commerce Workflow Order installer.
 */
class Installer implements InstallerInterface {

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdaterManagerInterface
   */
  protected $updateManager;

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $update_manager
  ) {
    $this->updateManager = $update_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->installOrderItemFields();
  }

  /**
   * Installs the fields required on order items.
   */
  protected function installOrderItemFields() {
    $definitions = OrderItemFieldHelper::baseFieldDefinitions();
    foreach ($definitions as $name => $definition) {
      $exists = $this->updateManager->getFieldStorageDefinition(
        $name,
        'commerce_order_item'
      );
      if ($exists) {
        continue;
      }

      $this->updateManager->installFieldStorageDefinition(
        $name,
        'commerce_order_item',
        'commerce_workflow_order',
        $definition
      );
    }
  }

}
