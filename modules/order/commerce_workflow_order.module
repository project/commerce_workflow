<?php

/**
 * @file
 * Hooks and functionality for the Commerce Workflow module.
 */

use Drupal\commerce_order\Entity\OrderItemTypeInterface;
use Drupal\commerce_workflow_order\Field\OrderItemFieldHelper;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Hooks.
 */

/**
 * Implements `hook_entity_base_field_info()`.
 */
function commerce_workflow_order_entity_base_field_info(
  EntityTypeInterface $entity_type
) {
  if ($entity_type->id() !== 'commerce_order_item') {
    return;
  }

  return OrderItemFieldHelper::baseFieldDefinitions();
}

/**
 * Implements `hook_form_FORM_ID_alter()`.
 */
function commerce_workflow_order_form_commerce_order_item_type_form_alter(
  array &$form,
  FormStateInterface $form_state
) {
  \Drupal::service('commerce_workflow_order.hook.order_item_type_form_alter')
    ->alterForm($form, $form_state);
}

/**
 * Implements `hook_form_FORM_ID_alter()`.
 */
function commerce_workflow_order_form_commerce_order_item_type_edit_form_alter(
  array &$form,
  FormStateInterface $form_state
) {
  \Drupal::service('commerce_workflow_order.hook.order_item_type_form_alter')
    ->alterForm($form, $form_state);
}

/**
 * Callbacks.
 */

/**
 * Entity building callback for order item type forms.
 */
function commerce_workflow_order__order_item_type__form_entity_builder(
  string $entity_type_id,
  OrderItemTypeInterface $order_item_type,
  array &$form,
  FormStateInterface $form_state
) {
  \Drupal::service('commerce_workflow_order.hook.order_item_type_form_alter')->buildEntity(
    $entity_type_id,
    $order_item_type,
    $form,
    $form_state
  );
}
