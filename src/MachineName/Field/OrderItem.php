<?php

namespace Drupal\commerce_workflow\MachineName\Field;

/**
 * Holds machine names of Order Item entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class OrderItem {

  /**
   * Holds the machine name for the state field.
   */
  const STATE = 'state';

}
